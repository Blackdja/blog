#!/bin/
bash
#We need to install dependencies only for Docker
[[ !
e /. dockerenv ]] && exit 0 set xe
# Install git (thephp image doesn't have it ) which is required by composer
apt-get update -yqq
apt-get install git -yqq
# Installphpunit , the tool that we will use for testing
curl --location --output / usr /local/ phpunit https:// phar.phpunit.de phpunit.phar
chmod +x /usr /local/ phpunit
# Install mysql driver # Here you can install any other extension that you need
docker-php-ext-install pdo_mysql